#!/bin/bash

set -e

OPTIONS=${1:-""}

find_parent(){
  local _parent
  if [ -f '.git' ]
  then
    _parent=$(awk '{print gensub("/\\.git/.*$","","g",$2)}' .git)
  else
    _parent=$(pwd)
  fi
  echo $_parent
}

parent=$(find_parent)
worktree=$(pwd)

if [ "$parent" != "$worktree" ]
then
  cd $parent
  if git worktree remove ${OPTIONS} $worktree 1>&2
  then
    echo $parent
  else
    echo $worktree
  fi
else
  echo $worktree
fi
