#!/bin/bash

GOTO_BRANCH=${1:-"main"}

# Get branch
_branch_line="branch refs/heads/${GOTO_BRANCH}"
_res=$(git worktree list --porcelain | grep -B 2 -xF "$_branch_line" | head -n 1 | awk '{print $2}')

if [ -z "${_res}" ]; then
  # maybe it's a tag?
  for _taginfo in $(git worktree list --porcelain | awk 'BEGIN{sha=""; tag=""; worktree=""} /^worktree/ {worktree=$2} /^HEAD/ {sha=$2} /^detached/ {print worktree "|" sha}')
  do
    _worktree=${_taginfo%|*}
    _sha=${_taginfo##*|}
    if [ "$(git tag --points-at $_sha)" == "$GOTO_BRANCH" ] ; then
      _res=${_worktree}
      break
    fi
  done
fi

echo $_res
