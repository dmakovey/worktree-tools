#!/bin/sh

TYPE=${1:-"all"}

if [ -e ${HOME}/.colors ]; then
  # File with ANSI color definitions
  # like https://github.com/droopy4096/home_setup/blob/master/.colors
  . ${HOME}/.colors
fi

get_local_branch(){
  git rev-parse --abbrev-ref HEAD
}

get_remote_branch(){
  git rev-parse --abbrev-ref --symbolic-full-name '@{u}' 2>/dev/null || echo ""
}

get_worktrees(){
  git worktree list | awk '{print $1}'
}

get_tags(){
  local _sha=$(git rev-parse --verify HEAD)
  git tag --points-at $_sha
}

sha_in_remote_tags(){
  local _sha=$1
  local _repo=${2:-"origin"}
  git ls-remote --tags ${_repo} 2>/dev/null | awk '{print $1}' | grep -qFx $_sha
}

in_remotes(){
  local _branch=$1
  local _repo=${2:-"origin"}
  git ls-remote -q --heads ${_repo} 2>/dev/null | awk '{print gensub("refs/heads/","","g",$2)}' | grep -qFx $_branch
}


print_status(){
  local _status=$1
  local _status_color=$2
  local _branch=$3
  local _wt=$4
  if [ ! -t 1 -o -n "$PS1" -o -z "$_status_color" -o -z "$RESET" ]; then
    echo "$_status $_branch $_wt"
  else
    /bin/echo -e "${_status_color}${_status}${RESET} $_branch $_wt"
  fi
}

delete_wt(){
  local _wt=$1
  local _branch=$2
  local _status_char=${3:-"-"}
  if [ "${TYPE}" == "delete" -o "${TYPE}" == "all" ]
  then
    print_status "$_status_char" "$Light_Red" "$_branch" "$_wt"
  fi
}

keep_wt(){
  local _wt=$1
  local _branch=$2
  if [ "${TYPE}" == "keep" -o "${TYPE}" == "all" ]
  then
    print_status "+" "$Light_Green" "$_branch" "$_wt"
  fi
}

local_wt(){
  local _wt=$1
  local _branch=$2
  if [ "${TYPE}" == "local" -o "${TYPE}" == "all" ]
  then
    print_status "?" "$Light_Blue" "$_branch" "$_wt"
  fi
}

tag_wt(){
  local _wt=$1
  shift
  local _tags="$@"
  if [ "${TYPE}" == "tag" -o "${TYPE}" == "all" ]
  then
    print_status "t" "$Light_Blue" "$_tags" "$_wt"
  fi
}

append(){
  local _what=$2
  local _where=$1
  local _how=${3:-" "}
  if [ -n "${_where}" ]; then
    echo "${_where}${_how}${_what}"
  else
    echo "$_what"
  fi
}

for wt in $(get_worktrees)
do
  cd $wt
  local_branch=$(get_local_branch)
  remote_branch_ref=$(get_remote_branch)
  tags=$(get_tags)
  if [ -n "${remote_branch_ref}" -a "${remote_branch_ref}" != '@{u}' ]
  then
    remote_name=${remote_branch_ref%/*}
    remote_branch=${remote_branch_ref#*/}
    if in_remotes $remote_branch $remote_name
    then
      keep_wt $wt $local_branch
    else
      delete_wt $wt $local_branch
    fi
  elif [ -n "${tags}" ]; then
    for _tag in ${tags}
    do
      _remotes=""
      for _remote in $(git remote)
      do
        _sha=$(git rev-parse $_tag)
        if sha_in_remote_tags $_sha  $_remote; then
          _remotes=$(append "$_remotes" "$_remote" ",")
        fi
      done
      if [ -n "$_remotes" ]; then
        delete_wt "$wt $_remotes" "$_tag" "_"
      else
        tag_wt "$wt $_remotes" "$_tag"
      fi
    done
  else
    local_wt $wt $local_branch
  fi
done
