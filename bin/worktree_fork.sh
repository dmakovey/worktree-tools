#!/bin/bash

UPDATE_FORK="no"

for opt in "$@"
do
  if [ "${opt:0:1}" == "-" ]; then
    case "${opt}" in
      "-u") UPDATE_FORK="yes"
            shift
            ;;
      "-f") FORCE_RECREATE="yes"
    esac
  else 
    break
  fi
done

FORK_URI=${1:-${FORK_URI}}
FORK_BRANCH=${2:-${FORK_BRANCH}}

# git@gitlab.com:equinix-ms/gitlab-charts/gitlab.git
_owner_uri=${FORK_URI%%/*}
_owner=${_owner_uri#*:}
FORK_PREFIX=${FORK_PREFIX:-${_owner}}

LOCAL_BRANCH="${FORK_PREFIX}-${FORK_BRANCH}"

set -e 

WORKTREE_REPO=${WORKTREE_REPO:-.}
REMOTE=${REMOTE:-""}
REMOTE_SPEC=""
REMOTE_SPEC=${REMOTE:+"${REMOTE}/"}
cd ${WORKTREE_REPO}

root_tree=$(git worktree list | head -n 1 | awk '{print $1}')
branch=${LOCAL_BRANCH}
branch_dir=$(echo ${branch} | tr "/" "_")

existing_branch=$(git worktree list --porcelain | grep -B 2 -xF "branch refs/heads/${branch}" || echo "")
if [ -n "${existing_branch}" ]
then
  existing_path=$(echo ${existing_branch} | awk '/^worktree/ { print $2 }')
  echo "Found existing worktree: ${existing_path}" >&2
  if [[ "${UPDATE_FORK}" == "yes" ]]; then
    pushd "${existing_path}" > /dev/null
    git fetch -q "${FORK_URI}" "${FORK_BRANCH}"
    git merge FETCH_HEAD >&2
    popd > /dev/null
  fi
  echo ${existing_path}
else
  cd ${root_tree} 
  # if [ -z "${SKIP_PULL}" ]; then git pull; fi
  git fetch -q "${FORK_URI}" "${FORK_BRANCH}"
  worktree_path=${root_tree}.${branch_dir}
  branch_exists="no"
  git worktree add -q -b ${LOCAL_BRANCH} ${worktree_path} FETCH_HEAD 1>&2 || branch_exists="yes"
  if [ "${branch_exists}" == "yes" ]; then
    if [[ "${FORCE_RECREATE}" == "yes" ]]; then
      retry="yes"
    else
      read -p "Should I try to force-delete '${LOCAL_BRANCH}' and retry (yes/no)? " retry 1>&2
    fi
    case "$(echo $retry | tr '[:upper:]' '[:lower:]')" in
        "y"|"yes")
            git branch -D ${LOCAL_BRANCH} >&2
            git worktree add -q -b ${LOCAL_BRANCH} ${worktree_path} FETCH_HEAD 1>&2 || branch_exists="yes"
            ;;
        "n"|"no")
            ;;
    esac
  fi
  echo "Created new worktree: ${worktree_path}" >&2
  echo ${worktree_path}
fi