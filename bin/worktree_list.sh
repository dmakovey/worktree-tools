#!/bin/bash

if [ -r "${HOME}/.colors" ]; then
  . "${HOME}/.colors"
fi

_dir_color=$Light_Blue
# _branch_color=$Green
_branch_color=$Dark_Gray
_hash_color=$Dark_Gray

FORMAT=${FORMAT:-"standard"}

for _repo_dir in "${@:-.}"
do
  pushd "${_repo_dir}" > /dev/null 2>&1 || exit 1
  case "${FORMAT}" in
    short)
      _awk_cmd='{ print $1}'
      ;;
    fancy)
      _awk_cmd='{ printf("%s\n\t%s\n\t%s\n", dc $1 r, hc $2 r, bc $3 r) }'
      ;;
    yaml)
      _awk_cmd='{ printf("\"%s\":\n  - sha: \"%s\"\n    branch: \"%s\"\n", $1, $2, gensub("\\]|\\[", "", "g", $3)) }'
      ;;
    *)
      _awk_cmd='BEGIN{maxdir=0} { if(length($1)>maxdir){maxdir=length($1)}; dirs[FNR]=dc $1 r; hashes[FNR]=hc $2 r; branches[FNR]=bc $3 r } END { for(i=1; i<=NR; i++) { printf("%-" maxdir "s\t%s\t%s\n", dirs[i], hashes[i], branches[i])}}'
      ;;
  esac

  if [ ! -t 1 -o -n "$PS1" -o -z "$_dir_color" -o -z "$RESET" ]; then
      case "${FORMAT}" in
        short|yaml)
          git worktree list | awk "${_awk_cmd}"
          ;;
        *)
          git worktree list
          ;;
      esac
  else
      git worktree list \
      | awk -v dc="${_dir_color}" -v bc="${_branch_color}" -v r="${RESET}" -v hc="${_hash_color}" \
        "${_awk_cmd}"
  fi
  popd > /dev/null 2>&1 || exit 1
done