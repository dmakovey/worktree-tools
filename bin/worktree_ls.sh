#!/bin/bash

LS_OPTIONS="${@}"
LS_DEFAULT_OPTIONS="--color -d"

for d in *
do
  if [ -d "${d}" ]
  then
    wt_dir=$(cd "$d" && git -C "." rev-parse --is-inside-work-tree >/dev/null 2>&1 && worktree_root.sh)
    echo ${wt_dir#"${PWD}"/}
  fi
done | sort -u | xargs ls ${LS_DEFAULT_OPTIONS} "${LS_OPTIONS}"
