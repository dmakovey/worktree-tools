#!/bin/bash
set -e

WORKTREE_REPO=${WORKTREE_REPO:-.}
REMOTE=${REMOTE:-""}
REMOTE_SPEC=""
REMOTE_SPEC=${REMOTE:+"${REMOTE}/"}
WORKTREE_OPTIONS=${WORKTREE_OPTIONS:-""}
cd ${WORKTREE_REPO}
_new_branch="0"

if [[ "$1" == "-n" ]]; then
	_new_branch="1"
	shift
fi

root_tree=$(git worktree list | head -n 1 | awk '{print $1}')
branch=${1:-${BRANCH}}
shift
if [ -n "$@" ]; then
	WORKTREE_OPTIONS=$@
fi
branch_dir=$(echo ${branch} | tr "/" "_")

existing_branch=$(git worktree list --porcelain | grep -B 2 -xF "branch refs/heads/${branch}" || echo "")
if [ -n "${existing_branch}" ]; then
	existing_path=$(echo ${existing_branch} | awk '/^worktree/ { print $2 }')
	echo "Found existing worktree: ${existing_path}" >&2
	echo ${existing_path}
else
	cd ${root_tree}
	# if [ -z "${SKIP_PULL}" ]; then git pull; fi
	git fetch --all -q 1>&2
	worktree_path=${root_tree}.${branch_dir}
	if [[ "$_new_branch" == "1" ]]; then
		git worktree add -q -b ${branch} ${WORKTREE_OPTIONS} ${worktree_path} 1>&2
	else
	    failed="no"
		git worktree add -q ${worktree_path} ${WORKTREE_OPTIONS} ${REMOTE_SPEC}${branch} 1>&2 || failed="yes"
		if [ ${failed} == "yes" ]; then
			echo "Having issues setting up worktree. Attempting a workaround using REMOTE spec..." >&2
			if [ -n "${REMOTE}" ]; then
				git branch ${branch}
				git branch -u ${REMOTE_SPEC}${branch} ${branch}
				git worktree add -q ${worktree_path} ${WORKTREE_OPTIONS} ${REMOTE_SPEC}${branch} 1>&2
			else
			    echo "No remote reference specified. Bailing out." >&2
				exit 1
			fi
		fi
	fi
	echo "Created new worktree: ${worktree_path}" >&2
	echo ${worktree_path}
fi
