#!/bin/bash
set -e 

WORKTREE_REPO=${WORKTREE_REPO:-.}
REMOTE=${REMOTE:-""}
REMOTE_SPEC=""
REMOTE_SPEC=${REMOTE:+"${REMOTE}/"}
cd ${WORKTREE_REPO}

root_tree=$(git worktree list | head -n 1 | awk '{print $1}')
branch=${1:-${BRANCH}}
shift
branch_dir=$(echo ${branch} | tr "/" "_")

IDE=${1:-${IDE:-"code"}}
if [ "$IDE" == "-" ]
then
  IDE="echo"
fi

existing_branch=$(git worktree list --porcelain | grep -B 2 -xF "branch refs/heads/${branch}" || echo "")
if [ -n "${existing_branch}" ]
then
  existing_path=$(echo ${existing_branch} | awk '/^worktree/ { print $2 }')
  echo "Found existing worktree: ${existing_path}"
  ${IDE} "$@" ${existing_path}
else
  cd ${root_tree} 
  # if [ -z "${SKIP_PULL}" ]; then git pull; fi
  git fetch --all
  worktree_path=${root_tree}.${branch_dir}
  git worktree add ${worktree_path} ${REMOTE_SPEC}${branch}
  echo "Created new worktree: ${worktree_path}"
  ${IDE} "$@" ${worktree_path}
fi

