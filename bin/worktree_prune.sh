#!/bin/bash

set -e

trap rm_wt EXIT

WORKTREE_STATUS_CMD=${WORKTREE_STATUS_CMD:-"worktree_status.sh"}
WORK_DONE_CMD=${WORK_DONE_CMD:-"worktree_close.sh"}

PRUNE_STATUS=${1:-"delete"}

START_DIR=${PWD}

if [ -n "${WORKTREE_LIST_FILE}" ] 
then
  EXISTING_LIST="yes"
else
  EXISTING_LIST="no"
fi

go_to_root(){
  local _parent
  if [ -f '.git' ]
  then
    _parent=$(awk '{print gensub("/\\.git/.*$","","g",$2)}' .git)
  else
    _parent=$(pwd)
  fi
  cd $_parent
}

go_to_root
WORKTREE_LIST_FILE=${WORKTREE_LIST_FILE:-$(mktemp -t worktree.XXXXXX)}

rm_wt(){
  cd ${START_DIR}
  if [ "$EXISTING_LIST" == "no" ]
  then
    rm ${WORKTREE_LIST_FILE}
  fi
}

if [ "$EXISTING_LIST" == "no" -o \( \( "$EXISTING_LIST" == "yes" \) -a \( ! -f "${WORKTREE_LIST_FILE}" \) \) ]
then
  {
    echo "## Following worktrees' status is ${PRUNE_STATUS}"
    echo "## Uncomment worktrees you'd like to delete"
  } > ${WORKTREE_LIST_FILE}

  $WORKTREE_STATUS_CMD ${PRUNE_STATUS} | sed 's/^[\+-\?_t] /# /g' >> ${WORKTREE_LIST_FILE}
fi

${EDITOR} ${WORKTREE_LIST_FILE}
for wt in $(sed -e 's/#.*$//g' ${WORKTREE_LIST_FILE}| awk '{print $2}')
do
  if [ ! -d $wt ] 
  then
    echo "Already removed $wt"
    continue
  fi
  cd $wt
  echo "==> Removing $wt"
  cd $(${WORK_DONE_CMD} $@)
  if [ "$wt" == "$PWD" ]
  then
    git status 
    read -p "Would you like to force removal (yes/no)? " retry
    case "$(echo $retry | tr '[:upper:]' '[:lower:]')" in
      y|yes)
        cd $(${WORK_DONE_CMD} --force $@)
        ;;
      n|no)
        ;;
    esac
  fi
done
