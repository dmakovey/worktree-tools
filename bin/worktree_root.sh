#!/bin/bash

get_root(){
  local _parent
  if [ -f '.git' ]
  then
    _parent=$(awk '{print gensub("/\\.git/.*$","","g",$2)}' .git)
  else
    _parent=$(pwd)
  fi
  echo $_parent
}

get_root