#!/bin/sh

function worktree(){
  CMD=${1}

  case ${CMD} in
    close|create|fork)
      shift
      new_dir=$(worktree_${CMD}.sh $@)
      if [[ -n "$new_dir" ]]; then
        cd ${new_dir}
      fi
      ;;
    open|status|prune|root|ls)
      shift
      worktree_${CMD}.sh "$@"
      ;;
    list)
      worktree_list.sh
      ;;
    go)
      case $2 in
        root)
          root_dir=$(worktree_root.sh)
          cd $root_dir
          ;;
        *)
          branch_dir=$(worktree_get.sh $2)
          cd $branch_dir
          ;;
      esac
      ;;
    help)
      echo "Available commands: open, close, create, fork, status, prune, list, ls, help"
      ;;
    *)
      worktree_open.sh "$@"
      ;;
  esac
}
