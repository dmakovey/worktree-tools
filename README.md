# worktree-tools

(Opinionated) Collection of wrapper tools for working with Git Worktrees

This collection is mainly geared for use with Zsh (see [worktree.zsh](#worktreezsh))

## Quickstart

add contents of `bin/` to your `PATH` (either copy into one of the folders already in the `PATH` or
add `$(pwd)/bin` to `PATH`

add convenience function and completion mechanism into `.zshrc`:

```shell
source /path/to/worktree-tools/worktree.zsh
```

after which one can use commands:

```shell
$ cd /some/repo/name
$ worktree create some-branch-name
$ pwd
/some/repo/name.some-branch-name
```

### Shortcut

one can add shortcut for the main command (assuming `worktree-tools` are located at `${HOME}/worktree-tools`):

```shell
PATH=${PATH}:${HOME}/worktree-tools/bin
. ${HOME}/worktree-tools/worktree.zsh

alias wt=worktree
compdef wt=worktree
```

## worktree.zsh

File to be included from `.zshrc` to initiate convenience shortcuts and completions

`worktree.zsh` is a convenience script for Zsh to be included from `.zshrc`:

```shell
. /path/to/worktree-tools/worktree.zsh
```

it wraps scripts from `bin/` via `worktree` function with multiple commands:

### close

executed from within active worktree. Closes current worktree and changes directory to parent git repo

### create

executed from within parent directory (or sibling worktree). Creates new worktree for specified branch and changes directory to it.

#### `-n`

create new branch and associtate it with a worktree

### fork

executed from within parent directory (or sibling worktree). Creates new worktree for specified fork of present repo and changes directory to it.

### get

executed from worktree directory. Return path to worktree for specified branch.

### open

Same as `create` but also opens new worktree in `IDE` of choice (default is `code`)

### prune

executed from parent git repo. Query all existing worktrees for the ones that have their upstream tracking branch gone.

### root

executed from worktree directory. Change directory to parent git repo
### status

query worktrees for `TYPE` of status:

* `delete` - suitable for deletion: upstream tracking branch specified but no longer exists
* `keep` - worktrees likely to be kept: upstream tracking branch specified and exists
* `local` - worktrees likely to be kept: no upstream tracking branch specified

### list

synonim to `git worktree list`

### worktree.sh

convenience wrapper script similar to `worktree.zsh` but lacking completion facilities. To be used from `.bashrc` or `.profile`:

```shell
source /path/to/worktree-tools/worktree.sh
```

### ls

list **root** worktrees in current directory