#!/bin/zsh

DEBUG=${DEBUG:-"no"}

if [[ "${DEBUG}" == "yes" ]]; then
  DEBUG_CMD="bash -x"
  set -x
else
  DEBUG_CMD=""
fi
_worktree_cmds=('close' 'open' 'create' 'fork' 'root' 'go' 'status' 'prune' 'list' 'ls' 'help')

function worktree(){
  CMD=${1}

  case ${CMD} in
    close|create|fork)
      shift
      new_dir=$(worktree_${CMD}.sh $@)
      if [[ -n "$new_dir" ]]; then
        cd ${new_dir}
      else 
        return 1
      fi
      ;;
    open|status|prune|root|ls)
      shift
      ${DEBUG_CMD} worktree_${CMD}.sh "$@"
      ;;
    list)
      shift
      worktree_list.sh $@
      ;;
    go)
      case $2 in
        root)
          root_dir=$(worktree_root.sh)
          cd $root_dir
          ;;
        *)
          branch_dir=$(worktree_get.sh $2)
          if [[ -n "${branch_dir}" ]]; then
            cd $branch_dir
          else
            return 1
          fi
          ;;
      esac
      ;;
    help)
      echo "Available commands: ${_worktree_cmds}"
      ;;
    *)
      worktree_open.sh "$@"
      ;;
  esac
}

# https://github.com/zsh-users/zsh-completions/blob/master/zsh-completions-howto.org
# https://www.dolthub.com/blog/2021-11-15-zsh-completions-with-subcommands/

function _worktree(){
  # New style completion with subcommands
  local line state
  _arguments -C '1: :->cmds' '*::arg:->args'
  case $state in
    cmds)
      compadd -X "Available commands:" $_worktree_cmds
      ;;
    args)
      case $line[1] in
        go)
          _go_cmd
          ;;
      esac
      ;;
  esac
}

function _go_cmd(){
  _alternative 'arguments:go:($(_list_of_worktrees))'
}

function _list_of_worktrees(){
  echo "root"
  # list branches:
  git worktree list --porcelain | grep branch | cut -d' ' -f2 | sed -e 's#refs/heads/##g'
  # list tags:
  git worktree list --porcelain | grep HEAD | cut -d' ' -f2 | xargs -n 1 git tag --points-at
}

compdef _worktree worktree

